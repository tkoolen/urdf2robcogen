/*
 * example_robot_anymal.cpp
 *
 *  Created on: Aug 29, 2017
 *      Author: depardo
 */

#include <urdf2robcogen/Urdf2RobCoGen.hpp>
#include <ros/package.h>

int main(int argc , char* arg[]) {

  // 1. set the path to your robot URDF
  // 2. set the names of the links using ONE of the following interfaces:
  //      setLinkNamesFromUrdf()
  //      setLinkNamesFromJointNames(std::vector<std::string> joint_names)
  //      setLinkNames(std::vector<std::string> link_names)
  // 3. set additional frames : Names, Parent Link, Transformation (translation and rotation)
  // 4. set the names of the end effectors (The end effector MUST be in the list of additional frames)
  bool debug_on = true;
  std::string my_robot_name = "ds_anymal";
  std::string path_to_robot_urdf = ros::package::getPath("urdf2robcogen") + "/urdf/"+my_robot_name + ".urdf";
  Urdf2RobCoGen myrobot(path_to_robot_urdf,debug_on);


  // 1.
  myrobot.setRobotName(my_robot_name);

  // 2.
  std::vector<std::string> joint_names = {
      "LF_HAA",
      "LF_HFE",
      "LF_KFE",
      "RF_HAA",
      "RF_HFE",
      "RF_KFE",
      "LH_HAA",
      "LH_HFE",
      "LH_KFE",
      "RH_HAA",
      "RH_HFE",
      "RH_KFE",
  };
  myrobot.setLinkNamesFromJointNames(joint_names);

  // 3.
  // ros::urdf does not have the concept of independent frames
  // therefore we manually add independent frames : names, parent link and transformations
  // this means that the user needs to know the names of the urdf-frames
  std::vector<std::string> frames = {
      "trunkCOM",
      "LF_FOOT",
      "RF_FOOT",
      "LH_FOOT",
      "RH_FOOT"
  };

  std::vector<std::string> frames_parent_links = {
      "base_link",
      "LF_SHANK",
      "RF_SHANK",
      "LH_SHANK",
      "RH_SHANK"
  };

  //ToDo: Add interfaces for adding Eigen vectors
  urdf::Vector3 vbase_t(0.022,-0.01,0.01);
  double x = 0.3411;
  double y = 0.0191;
  double z = -0.0038;
  urdf::Vector3 lf_t(-y,x,z); //ok
  urdf::Vector3 rf_t(-y,x,-z);//ok
  urdf::Vector3 lh_t(y,x,z);
  urdf::Vector3 rh_t(y,x,-z);

  std::vector<urdf::Vector3> frames_translation = {
      vbase_t,
      lf_t,
      rf_t,
      lh_t,
      rh_t
  };

  urdf::Vector3 vbase_r(0.0, 0.0 , 0.0);
  urdf::Vector3 lf_r(0.323, 0.549, 0.0);
  urdf::Vector3 rf_r(0.323, 0.549, 0.0);
  urdf::Vector3 lh_r(0.323, 0.549, 0.0);
  urdf::Vector3 rh_r(0.323, 0.549, 0.0);

  std::vector<urdf::Vector3> frames_rotation = {
      vbase_r,
      lf_r,
      rf_r,
      lh_r,
      rh_r
  };
  myrobot.setAdditionalFrames(frames,frames_parent_links,frames_translation,frames_rotation);


  //4.
  myrobot.setEENames(frames);


  myrobot.generateFiles();

  return 0;
}
