/*
 * toy_example_2D.cpp
 *
 *  Created on: Sept 19, 2017
 *      Author: depardo
 */

#include <urdf2robcogen/Urdf2RobCoGen.hpp>
#include <ros/package.h>

int main(int argc , char* arg[]) {

  // 1. set the path to your robot URDF
  // 2. set the names of the links using ONE of the following interfaces:
  //      setLinkNamesFromUrdf()
  //      setLinkNamesFromJointNames(std::vector<std::string> joint_names)
  //      setLinkNames(std::vector<std::string> link_names)
  // 3. set additional frames : Names, Parent Link, Transformation (translation and rotation)
  // 4. set the names of the end effectors (The end effector MUST be in the list of additional frames)

  std::string my_robot_name = "planar_2DoF";
  std::string path_to_robot_urdf = ros::package::getPath("urdf2robcogen") + "/urdf/"+my_robot_name + ".urdf";
  Urdf2RobCoGen myrobot(path_to_robot_urdf);

  // Example loading the URDF from the parameter server
  //    ros::init(argc,arg,"urdf_parser");
  //    std::string parameter_name = "/robot_description";
  //    Urdf2RobCoGen myrobot(true,parameter_name);

  // 1.
  myrobot.setRobotName(my_robot_name);

  // 2.
  myrobot.setLinkNamesFromUrdf();

  // 3.
  // ros::urdf does not have the concept of independent frames
  // therefore we manually add independent frames : names, parent link and transformations
  // this means that the user needs to know the names of the urdf-frames
  // ToDo: method for creating frames at the end of the branch/tree
  std::vector<std::string> frames = {
      "HAND_frame"
  };

  std::vector<std::string> frames_parent_links = {
      "lower_arm"
  };

  //ToDo: Add interfaces for adding Eigen vectors
  urdf::Vector3 hand_t(0.3,0,0);

  std::vector<urdf::Vector3> frames_translation = {
      hand_t
  };

  urdf::Vector3 hand_r(0.0, 0.0, 0.0);

  std::vector<urdf::Vector3> frames_rotation = {
      hand_r
  };
  myrobot.setAdditionalFrames(frames,frames_parent_links,frames_translation,frames_rotation);


  //4.
  myrobot.setEENames(frames);


  myrobot.generateFiles();

  return 0;
}
