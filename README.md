# URDF2ROBCOGEN #

### What is this repository for? ###

This tool generates the RobCoGen description files (.kindsl & .dtdsl) from URDF files or a parameter in the ros Parameter Server.

#### Main differences between URDF and RobCoGen ####

+ URDF:
	* Frame Rotations are Extrincic (rotations about a fixed frame)
	* The axis of rotation of the Joints is set with the vector property 'axis' of the joint element
	* There is no concept of **Frame**
	* Artificial links are used to define frames (a.k.a., link\_frames)
	* Artificial Joints are used to connect link\_frames to the real links
	* It is common to find URDF with 'virtual' links for compatibility with other tools
	* Virtual links might have incomplete information (i.e., inertial parameters)
	* The ineratial parameters are expressed w.r.t a frame at the CoM of the link
	
+ RobCoGen:
	* Frame Rotations are Intrinsic (rotations about the current frame)
	* Frame Rotation order is x,y,z
	* The axis of rotation of the Joint is always Z
	* Additional frames can be defined at any link
    * Inertial parameters are expressed w.r.t. the link frame (i.e., the corresponding joint frame)
	* It includes a special feature to set a different frame for expressing the inertial parameters (**ref\_frame**)

This tool covers three different use cases: 

1. User wants to translate the origianl URDF, including virtual links.

    ```
	setLinkNamesFromUrdf()
	```
	
2. User knows the list of joints that connect the real links.

    ```
    setLinkNamesFromJointNames(std::vector<std::string> joint_names)
    ```

3. User knows the list of real links.

    ```
    setLinkNames(std::vector<std::string> link_names)
    ```

4. It is mandatory to define the frames manually using the follwoing interface

    ```
    setAdditionalFrames(const std::vector<std::string> & frame,  
    const std::vector<std::string> & parents,  
    const std::vector<urdf::Vector3> & translation,    
    const std::vector<urdf::Vector3> & rotation)  
    ```

5. It is mandatory to include a frame at the end effector (Temporal!)


The generated files might need to be completed by hand in case of incomplete and ambiguous information in the URDF (e.g, missing inertia parameters, etc).

### Dependencies ###

* Eigen
* ros::urdf
* ros::roslib
* [kindr](https://github.com/ethz-asl/kindr) (a kinematics library for robotics)

### Reading URDFs ###

1. You can read the URDF from files

    ```
    Urdf2RobCoGen(const std::string & path_to_urdf_file)
    ```

2. Additionally, you can load the URDF from the parameters server

    ```
    Urdf2RobCoGen(const bool & parameter_server , const std::string & param_name)
    ```
    

### Examples ###

There are two examples available :

1. example_robot_anymal (in case YOU have anymal's urdf)
2. toy_example_2D

### Who do I talk to? ###

Diego Pardo (depardo@ethz.ch)
